#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/js-d3-flame-graph/Sanity/upstream-testsuite
#   Description: Upstream sanity testsuite
#   Author: Jan Kuřík <jkurik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2020 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="js-d3-flame-graph"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm "${PACKAGE}" ||\
            rlDie "Can not recover from the previous error"
        rlFileBackup --missing-ok "/root/.npm" "/root/.bashrc"
        rlRun "rm -rf /root/.npm"
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd ${TmpDir}"
    rlPhaseEnd

    rlPhaseStartTest "Prepare ${PACKAGE} for testing"
        rlFetchSrcForInstalled "${PACKAGE}" \
            || rlDie "Can not download SRPM of ${PACKAGE} - giving up"
        rlRun "rpm -D \"_topdir ${TmpDir}\" -U \
            $(rpm -q --qf '%{name}-%{version}-%{release}.src.rpm' ${PACKAGE}.noarch)"
        rlRun "rpmbuild --nodeps -D \"_topdir ${TmpDir}\" \
            -bp ${TmpDir}/SPECS/${PACKAGE}.spec"
    rlPhaseEnd

    rlPhaseStartTest "Run the upstream testsuite"
        BDIR=$(rpm -q --qf '%{name}-%{version}' ${PACKAGE}.noarch)
        P=$(sed 's/^js-//' <<< "${BDIR}")
        if rlRun "pushd ${TmpDir}/BUILD/${P}/test"; then
            rlRun "npm run test"
            rlRun "popd"
        else
            rlFail "Can not find the working directory of the upstream testsuite"
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -rf ${TmpDir}" 0 "Removing tmp directory"
        rlRun "rm -rf /root/.npm"
        rlFileRestore
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
